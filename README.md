# Gitlab helper to upload from gitlab CI into OBS

This project contains a helper to upload Debian packages from git into
Collaboras OBS instance.

The git repository must be setup such that a debian source package can be build
from it (either using pristine-tar, pristine-lfs of native packages) or the
source package job needs to be overridden to build a source package.

# Setup

First of all a user on OBS is required which is able to upload packages to the
project of choice. This should be a dedicated "bot" type user rather then a
real user.

For ci-to-obs.v2.yml the credentials of this user should be configured as CI variables
as such:
* `OBS_USER` OBS username to use for the upload
* `OBS_PASSWORD` OBS password to use for the upload

For ci-to-obs.v1.yml (deprecated) the credentials of this user should be configured as CI variables
as such:
* `OSC_USERNAME` OBS username to use for the upload
* `OSC_PASSWORD` OBS password to use for the upload

Both of these should be marked as *protected* and at least the password should
be marked as *masked*. To determine  what gets uploaded there are multiple
options:

## Uploading all commits from the default branch

If the `OBS_PROJECT` variable is  then for every commit on the *default*
gitlab branch a package will be uploaded to the OBS project configured in this
variable.

## Uploading based on tags

If the `OBS_UPLOAD_TAG_PREFIX` variable is set then for any tag matching the
prefix configured the package will be uploaded. The project to upload to should
be configured by setting the `TAGS_OBS_PROJECT` variable.

## Uploading non-default branches

To upload from other branches then the default set the `OBS_UPLOAD_BRANCHES`
variable; This can be a regexp to match multiple branches.

For each branch the target OBS project needs to be configured; This can be done
by setting a variable matching the branch' slug suffixed with `_OBS_PROJECT`
and `-` replaced by `_`. e.g. for a branch called `wip/newfeature` the gitlab
slug would be wip-newfeature, hence the variable `wip_newfeature_OBS_PROJECT`
should be set with the target OBS project.

# gitlab-ci.yml setup

For projects which are already using gitlab ci the ci-to-obs.v2.yml can be
included as such:
```
include:
  - project: 'obs/ci-to-obs'
    file: '/ci-to-obs.v2.yml'
    ref: 'main'
```

If a project isn't using gitlab ci yet a "Custom CI configuration path" can be
configured as well in the settings as `ci-to-obs.v2.yml@obs/ci-to-obs`. This
avoids the need of a gitlab ci configuration file directly in the repository.

# Package versioning

To determine the source package version the deb-git-version-gen tool is used
from [deb-build-snapshot](https://salsa.debian.org/smcv/deb-build-snapshot)
with the `--guess-release` parameter. This ensures that each build has a unique
and increasing name (assuming the branch is not rebased).

To get pretty package version one can use tags matching the `debian/changelog`
version.
