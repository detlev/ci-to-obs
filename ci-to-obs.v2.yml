variables:
  NAME: ${GITLAB_USER_NAME}
  EMAIL: ${GITLAB_USER_EMAIL}
  GIT_AUTHOR_NAME: ${GITLAB_USER_NAME}
  GIT_COMMITTER_NAME: ${GITLAB_USER_NAME}
  CI_AUTH_PROJECT_URL: https://${GITLAB_CI_USER}:${GITLAB_CI_PASSWORD}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git/
  GBP_CONF_FILES: "/dev/null"
  # Disable shallow clone as history is needed for describing tags
  GIT_DEPTH: 0
  OBS_PROJECT: "You need to set a OBS project to upload to"
  OBS_SERVER: https://build.collabora.com
  CI_BUILDER_IMAGE: ${CI_REGISTRY}/obs/ci-to-obs/package-source-builder:main

  # Maximum size in bytes of build log displayed in the "live" job view
  MAX_BUILDLOG_SIZE: 3000000
  # Size of logs in bytes to display from the end of build log in case it has been truncated
  END_BUILDLOG_SIZE: 10000

  OBS_RUNNER_TAG: obs-runner

image: ${CI_BUILDER_IMAGE}

stages:
  - build
  - upload
  - OBS

build debian source:
  variables:
    GIT_STRATEGY: clone
  stage: build
  tags:
    - lightweight
  before_script:
    - mkdir -p .git/info
    - echo '* -text -eol -crlf -ident -filter -working-tree-encoding -export-subst' > .git/info/attributes
    - |
      if [ -f .gitattributes ]
      then
        sed -n "/ filter=lfs/p" .gitattributes >> .git/info/attributes
      fi
    - |
      if [ -n "${TAGS_OBS_PROJECT}" -a -n "$CI_COMMIT_TAG" ] ; then
        export UPLOAD_TO=${TAGS_OBS_PROJECT}
      elif [ -n "$OBS_PROJECT" -a "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" ] ; then
        export UPLOAD_TO=${OBS_PROJECT}
      elif [ -n "$CI_COMMIT_BRANCH" ]; then
        OBS_PROJECT_VAR=${CI_COMMIT_REF_SLUG//-/_}_OBS_PROJECT
        export UPLOAD_TO=${!OBS_PROJECT_VAR}
        if [ -z "${UPLOAD_TO}" ] ; then
          echo "💔 ${OBS_PROJECT_VAR} is not set"
        fi
      fi

      if [ -z "${UPLOAD_TO}" ] ; then
        echo "No upload location for this run!"
      else
        echo "UPLOAD_TO=$UPLOAD_TO" | tee -a build.env
      fi
  script:
    - ci-buildpackage -S --git-prebuild='debian/rules debian/control || true'
    - DSC=$(echo _build/*.dsc)
    - echo "DSC=$DSC" | tee -a build.env
  artifacts:
    paths:
      - _build
    when: always
    reports:
      dotenv: build.env
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^upstream\//
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^debian\//
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^pristine-\//
      when: never
    - if: $CI_COMMIT_BRANCH
      when: on_success
    - when: never

upload to obs:
  stage: upload
  interruptible: false
  tags:
    - $OBS_RUNNER_TAG
  resource_group: obs-$CI_COMMIT_REF_NAME
  # Clear the default value, which uses shell syntax.
  before_script: []
  script:
    - >
      dput $UPLOAD_TO $DSC
        --rebuild-if-unchanged=true
    - >
      generate-monitor $OBS_RUNNER_TAG
        --artifact-expiration '3 days'
        --download-build-results-to 'results/'
  after_script:
    - prune --ignore-missing-build-info --only-if-job-unsuccessful
  artifacts:
    paths:
      - build-info.yml
      - obs.yml
    when: always
  environment:
    name: upload/$CI_COMMIT_REF_NAME
    # Ideally this would point to the obs project,
    # but we can't mangle variables enough
    url: https://build.collabora.co.uk
  rules: &upload_rules
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - if: '$TAGS_OBS_PROJECT && $CI_COMMIT_TAG && $OBS_UPLOAD_TAG_PREFIX =~ $CI_COMMIT_TAG'
    - if: '$CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH =~ $OBS_UPLOAD_BRANCHES'
    - if: '$OBS_PROJECT && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
  needs:
    - build debian source

obs:
  stage: OBS
  needs:
    - "upload to obs"
  resource_group: obs-$CI_COMMIT_REF_NAME
  trigger:
    strategy: depend
    include:
      - artifact: obs.yml
        job: "upload to obs"
  rules: *upload_rules
