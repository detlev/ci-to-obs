#!/bin/sh
set -ve

if [ -z "$1" -o -z "$2" -o -z "$3" -o -z "$4" ] ; then
  echo "Usage: $0 project package repository architecture"
  exit 1
fi

PROJECT=$1
PACKAGE=$2
REPOSITORY=$3
ARCHITECTURE=$4

OSC_RESULTS="osc results \
    --verbose              \
    ${PROJECT}             \
    ${PACKAGE}             \
    --repo=${REPOSITORY}   \
    --arch=${ARCHITECTURE} \
    --csv"

echo Monitoring ${PROJECT} ${PACKAGE} ${REPOSITORY} ${ARCHITECTURE}
echo 'Waiting for the build to begin...'
# ignore intermediate states while the build has not started yet
while ${OSC_RESULTS} --format='%(dirty)s|%(code)s' | grep -q '^True\|scheduled$'
  do
    ${OS_RESULTS}
    sleep 1
  done

${OSC_RESULTS} --format='%(code)s|%(state)s|%(details)s'
${OSC_RESULTS} --format='%(code)s|%(state)s|%(details)s' | grep -E '^building|^succeeded'
osc remotebuildlog ${PROJECT} ${PACKAGE} ${REPOSITORY} ${ARCHITECTURE} | tee build.log

# ignore intermediate states while the build has not been completed yet
while ${OSC_RESULTS} --format='%(dirty)s' | grep -q True ; do
  ${OSC_RESULTS}
  sleep 1
done

${OSC_RESULTS} --format='%(code)s|%(repository)s|%(arch)s|%(state)s|%(dirty)s||%(details)s' | tee result.txt
grep '^succeeded' result.txt
